(defproject sap-concur "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main sap-concur.core
  :plugins [[olical/lein-transcriptor "1.0.0"]]
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]
                 [compojure "1.6.1"]
                 [funcool/cats "2.2.0"]
                 [cheshire "5.8.0"]
                 [ring/ring-json "0.4.0"]
                 [com.cognitect/transcriptor "0.1.5"]
                 [clj-time "0.14.4"]
                 [clj-http "3.9.0"]])
