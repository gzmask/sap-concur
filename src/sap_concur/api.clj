(ns sap-concur.api
  (:require [compojure.core :as w]
            [sap-concur.items :as items]
            [clojure.spec.alpha :as s]
            [clj-time.core :as time]
            [clj-time.format :as time.fmt]
            [cats.core :as m]
            [cats.monad.either :as e]
            [ring.util.response :as resp]))

(def ^:const SKEW-MIN-ALLOW 1)
(def items (atom []))

(s/def ::id int?)
(s/def ::timestamp string?)
(s/def ::item (s/keys :req-un [::id ::timestamp]))
(s/def ::req-item (s/keys :req-un [::item]))
(s/def ::items (s/coll-of ::req-item))

(defn- validate-req [req-item]
  (if (s/valid? ::req-item req-item)
    (e/right req-item)
    (e/left (str "Spec invalid for " req-item))))

(defn- check-clock-skew
  "Check if timestamp is skewed."
  [{{time :timestamp} :item :as req-item}]
  (let [time* (time.fmt/parse time)
        interval (time/interval (time/minus (time/now) (time/minutes SKEW-MIN-ALLOW))
                                (time/plus (time/now) (time/minutes SKEW-MIN-ALLOW)))]
    (if (time/within? interval time*)
      (e/right req-item)
      (e/left "Clock skew error."))))

(defn- update-store [item]
  (try
    (e/right (swap! items items/add item))
    (catch Exception e
      (e/left (str "Update failed:" (.getMessage e))))))

(def sap-concur-app
  (w/routes
   (w/GET "/items" request
          (resp/response @items))
   (w/POST "/items" {item :body}
           (e/branch (m/mlet [_ (validate-req item)
                              _ (check-clock-skew item)
                              _ (update-store item)]
                             (m/return "Item created."))
                     #(-> (str "Creation failed: " %)
                          resp/response
                          (resp/header "Content-Type" "text/html")
                          (resp/status 500))
                     #(-> (resp/response %)
                          (resp/header "Content-Type" "text/html")
                          (resp/status 201))))))
