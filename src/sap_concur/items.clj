(ns sap-concur.items
  (:require [clj-time.core :as t]
            [clj-time.format :as f]))

(def ^:const TIME-INTERVAL 2000)
(def ^:const ITEM-LIMIT 100)

(defn- ->time [item]
  (->> item
       :item
       :timestamp
       (f/parse (f/formatter :date-time))))

(defn- more-than-n-ms?
  [ms items]
  (let [times    (map ->time
                      items)
        interval (t/interval (last times) (t/now))]
    (< ms (t/in-millis interval))))

(defn prune-items
  "Removed items"
  [items]
  (if (and (more-than-n-ms? TIME-INTERVAL items)
          (< ITEM-LIMIT (count items)))
    (recur (rest items))
    items))

(defn prune-items-t
  "A transducer version of prune-items."
  [items]
  (sequence
   (keep-indexed #(if (or (< %1 ITEM-LIMIT)
                          (-> %2
                              ->time
                              (t/interval (t/now))
                              t/in-millis
                              (< TIME-INTERVAL)))
                    %2)) items))

(defn add
  "Add most recent item to the head position of the coll"
  [items item]
  (prune-items-t (cons item items)))
