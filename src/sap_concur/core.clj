(ns sap-concur.core
  (:require [sap-concur.api :as api]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.adapter.jetty :refer [run-jetty]]))

(defn -main [& args]
  (let [app (run-jetty (-> api/sap-concur-app
                           wrap-json-response
                           (wrap-json-body  {:keywords? true}))
                       {:port  3000
                        :join? false})]
    (println "Server ready...")
    (.addShutdownHook (Runtime/getRuntime)
                      (Thread. #(do (println "Shutting down...")
                                    (.stop app))))))
